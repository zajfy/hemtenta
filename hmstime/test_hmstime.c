#include <stdio.h>
#include <string.h>

#include "hmstime.h"

int main(void) {
    /* "konstruktor" hms: */
    hmstime time1 = hms(12,06,18);
    hmstime time2 = hms(12,06,18);

    /* hms_equal: */
    if(!hms_equal(time1, time2)) {
       printf("Fel hms(12,06,18) != hms(12,06,18)\n");
    }

    /* hms_diff: */
    if(!hms_equal(hms_diff(time1, time2), hms(0,0,0))) {
       printf("Fel på hms_diff!\n");
    }
    hmstime diff = hms_diff(hms(12,33,14), hms(11,56,12));
    if(!hms_equal(diff, hms(0,37,2))) {
       printf("Fel på hms_diff\n");
    }

    return 0;
}