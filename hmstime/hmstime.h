#ifndef _HMSTIME_H
#define _HMSTIME_H

/* DESCRIPTION: hmstime stores a daytime */
typedef struct hmstime_S {
   // skriv innehåll här!
   int hour;
   int minute;
   double second;
} hmstime;

/* skriv förklarande kommentar här!
	Med inmatningen hour, minute och second så sätter 
	den varje värde på rätt plats i structen temp av typen hmstime
	Därefter returneras structen.
 */
hmstime hms(int hour, int minute, double second);

/* skriv förklarande kommentar här!
	Funktioen jämnför de värden som finns i de två structerna time1 och time2 av typen hmstime
 */
int hms_equal(hmstime time1, hmstime time2);

/* skriv förklarande kommentar här!
	Funktionen beräknar skilladerna mellan time1 och time2 på hour, minute och second.
 */
hmstime hms_diff(hmstime time1, hmstime time2);

#endif
