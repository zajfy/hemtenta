#include "hmstime.h"
#include <math.h>
#include <stdbool.h>
#include <stdio.h>

/* implementera de tre funktionerna i hmstime.h här
   nedanför!
 */
 
 #define EPSILON 1e-9

 //checks if 2 doubles are equal
//https://stackoverflow.com/questions/11747251/comparing-double-values-in-c
bool dequals(double x, double y)
{
	return fabs(x-y) < EPSILON;
}
//TODO: check if userinput is wrong
//Assuming the input is correct, convert time out of bounds to correct format.
//Also used for difference correction
//e.g 12:95:12 becomes 13:35:12 or 12:40:-15 becomes 11:39:45
hmstime time_bounds(hmstime time)
{
	if(time.second < 0.0)
	{
		time.second = 60.0 + time.second; 
		time.minute--;
	}
	else if(time.second > 59.9)
	{
		time.second = time.second - 60.0;
		time.minute++;
	}
	if(time.minute < 0)
	{
		time.minute = 60 + time.minute;
		time.hour--;
	}
	else if(time.minute > 59)
	{
		time.minute = time.minute - 60;
		time.hour++;
	}
	return time;
}

hmstime hms(int hour, int minute, double second)
{
	hmstime temp;
	temp.hour = hour;
	temp.minute = minute;
	temp.second = second;
	temp = time_bounds(temp);
	
	return temp;
}

int hms_equal(hmstime time1, hmstime time2)
{
	if(time1.hour == time2.hour && time1.minute == time2.minute && dequals(time1.second, time2.second))
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

hmstime hms_diff(hmstime time1, hmstime time2)
{
	hmstime temp;
	temp.hour = time1.hour - time2.hour;
	temp.minute = time1.minute - time2.minute;
	temp.second = time1.second - time2.second;
	temp = time_bounds(temp);
	return temp;
}

