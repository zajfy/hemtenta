#include <stdio.h>

#include "mystring.h"
#include "print_comparison.h"

int main(void) {
    mystr *str1 = mystr_new("Hej!");
    mystr *str2 = mystr_new("Hej!");
    print_comparison(mystr_compare(str1, str2), "equal");
    str1 = mystr_new("Abram");
    str2 = mystr_new("Abraham");
    print_comparison(mystr_compare(str1, str2), "greater");
    str1 = mystr_new("Abra");
    str2 = mystr_new("Abrakadabra");
    print_comparison(mystr_compare(str1, str2), "less");
    str1 = mystr_new("Adam");
    str2 = mystr_new("Bertil");
    print_comparison(mystr_compare(str1, str2), "less");
    printf("--- tests of strdup and length ---\n");
    int n = mystr_len(str2);
    printf("%d == 6?\n", n);
    str2 = mystr_dup(str1);
    print_comparison(mystr_compare(str1, str2), "equal");
    return 0;
}