void print_comparison(int compval, char *expect) {
    printf("%s == ", expect);
    switch(compval) {
      case -1:
        printf("less\n"); break;
      case 0:
        printf("equal\n"); break;
      case 1:
        printf("greater\n"); break;
      case -999:
        printf("something odd occurred\n"); break;
      default:
        printf("something bizarre occurred\n"); break;
    }
}
